/*
1. Наведіть кілька прикладів, коли доречно використовувати в коді конструкцію try...catch.
    Конструкцію try...catch доцільно використовувати, коли передбачається можливівсть виникнення помилки
    Наприклад з бекенду прийшли невірні/неповні дані, використання яких призведе до помилки і припинення виконання скрипту.
    Щоб цього уникнути варто використовувати в коді конструкцію try...catch, яка дозволить обробити помилку в блоці try, виконати 
    інший код у блоці catch та продовжити виконання скрипту далі.
    Помилки також можна логувати, відправляти на сервер, що дозволить їх обробити та усунути їх причини в подальшому
*/

"use strict";

const books = [
    { 
      author: "Люсі Фолі",
      name: "Список запрошених",
      price: 70 
    }, 
    {
     author: "Сюзанна Кларк",
     name: "Джонатан Стрейндж і м-р Норрелл",
    }, 
    { 
      name: "Дизайн. Книга для недизайнерів.",
      price: 70
    }, 
    { 
      author: "Алан Мур",
      name: "Неономікон",
      price: 70
    }, 
    {
     author: "Террі Пратчетт",
     name: "Рухомі картинки",
     price: 40
    },
    {
     author: "Анґус Гайленд",
     name: "Коти в мистецтві",
    }
];

const root = document.querySelector("#root");

function booksToSite(arr, domEl) {
    let list = document.createElement("ul");
    let count = 0;
    arr.forEach(obj => {
        try { 
            if (!obj.author) {
                throw new Error(`Відсутня властивість Author`);
            }
            if (!obj.name) {
                throw new Error("Відсутня властивість Name");
            }
            if (!obj.price) {
                throw new Error("Відсутня властивість Price");
            }
            let book = document.createElement("li");
            count++
            book.innerHTML = `Book ${count}
            <ul>
                <li>Author: ${obj.author}</li>
                <li>Name: ${obj.name}</li>
                <li>Price: ${obj.price}</li>
            </ul>
            `;
            list.append(book);
        } catch (err) {
            console.log(err.message);
        }
        
    });
    domEl.append(list);
}

booksToSite(books, root);
